// import Vue from "vue";
// 2 примера завода App
// const App = {
Vue.createApp({
    data() {
        return {
            myHtml: '<h1>myHtml in app.js</h1>',
            placeholderString: "Введите название заметки",
            title: "Список заметок",
            inputValue: "",
            notes: ['Заметка 1', 'Заметка 2'],
            person: {
                firstName: 'Aleks',
                lastName: 'Romanov',
                age: 35,
            },
            // items: [1, 2, 3, 4, 5, 6]
            items: [1, 2]
        }
    },
    methods: {
        remove(i) {
            this.items.splice(i, 1);
        },
        addItem() {
            this.items.unshift(this.$refs.myInput.value);
            this.$refs.myInput.value = '';
        },
        // Заменяем на v-model на уровне выше в index.html
        // inputChangeHandler(event) {
        //     this.inputValue = event.target.value;
        // },
        stopPropagation(event) {
            event.stopPropagation();
        },
        addNewNote() {
            if (this.inputValue !== "") {
                this.notes.push(this.inputValue);
                this.inputValue = "";
            }
        },
        toUpperCase(item) {
            return item.toUpperCase();
        },
        deleteNote(index) {
            this.notes.splice(index, 1);
        }
    },
    computed: {
        evenItems() {
            return this.items.filter(i => i)
            // return this.items.filter(i => i % 2 === 0)
        },
        doubleCountComputed() {
            return this.notes.length * 2;
        }
    },
    watch: {
        inputValue(value) {
            if (value.length > 10) {
                this.inputValue = "";
            }
        }
    }
}).mount('#app')


// Vue.createApp(App).mount('#app')

// Вторая запись data по ES6:
// Vue.createApp({
//     data: () => {},
// data() {
//     return {
//
//     }
// }
// }).mount('#app')
